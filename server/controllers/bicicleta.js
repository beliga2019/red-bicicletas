var Bicicleta = require ('../models/bicicleta');

exports.bicicleta_list = (req, res) =>{
    res.render('bicicleta/index', {bicis: Bicicleta.allBicis});
}

exports.bicicleta_create_get = (req, res) => {
    res.render('bicicleta/create');
}

exports.bicicleta_create_post = (req, res) => {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];
    Bicicleta.add(bici);

    res.redirect('/bicicleta');
}

exports.bicicleta_delete_post = (req, res) =>{
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicleta');
}

exports.bicicleta_update_get = (req, res) => {
    var bici = Bicicleta.findById(req.params.id);

    res.render('bicicleta/update', {bici});
}

exports.bicicleta_update_post = (req, res) => {
    var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;   
    bici.ubicacion = [req.body.lat, req.body.long];
 

    res.redirect('/bicicleta');
}