var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
 }

 Bicicleta.prototype.toString = () => {
     return 'id: ' + this.id + " | color: " + this.color;
 }

 Bicicleta.allBicis = [];
 Bicicleta.add = (aBici) => {
     Bicicleta.allBicis.push(aBici);
 }

 Bicicleta.findById = (aBiciId) => {
     var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
     if (aBici)
        return aBici;
     else
        throw new Error (`No existe una Bicicleta con el id ${aBiciId}`); 

 }

 Bicicleta.removeById = (aBiciId) => {
     Bicicleta.findById(aBiciId);
     for(var i = 0; i < Bicicleta.allBicis.length; i++){
         if(Bicicleta.allBicis[i].id == aBiciId){
             Bicicleta.allBicis.splice(i, 1);
             break;
         }
     }
 }
 var a = new Bicicleta(1, 'rojo', 'urbana', [-31.108849, -64.483856]);
 var b = new Bicicleta(2, 'azul', 'montaña', [-31.101325, -64.465012]);

 Bicicleta.add(a);
 Bicicleta.add(b);

 module.exports = Bicicleta;