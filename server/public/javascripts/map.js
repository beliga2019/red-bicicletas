var mymap = L.map('main_map').setView([-31.095920, -64.480964], 13);


L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoidmFuaWFyODYiLCJhIjoiY2tlaHVzYTQ0MHoycDJzcnp5M2ZoaTd1eiJ9.u38VjvwnUIbAvNnFKSK6Eg'
}).addTo(mymap);



$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: (result) => {
        console.log(result);
        result.bicicletas.forEach((bici) => {
            L.marker(bici.ubicacion, {title : bici.id}).addTo(mymap);
        
        });
    }
})